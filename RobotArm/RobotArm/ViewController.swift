//
//  ViewController.swift
//  RobotArm
//
//  Created by Nikolay Kovachev on 22.09.19.
//  Copyright © 2019 GoStartups. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var Motor1Label: UILabel!
    @IBOutlet weak var Motor2Label: UILabel!
    @IBOutlet weak var Motor3Label: UILabel!
    @IBOutlet weak var Motor4Label: UILabel!
    @IBOutlet weak var Motor1Slider: UISlider!
    @IBOutlet weak var Motor2Slider: UISlider!
    @IBOutlet weak var Motor3Slider: UISlider!
    @IBOutlet weak var Motor4Slider: UISlider!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Motor1Label.text = "Motor1 = " + String(Int(Motor1Slider.value))
        Motor2Label.text = "Motor2 = " + String(Int(Motor2Slider.value))
        Motor3Label.text = "Motor3 = " + String(Int(Motor3Slider.value))
        Motor4Label.text = "Motor4 = " + String(Int(Motor4Slider.value))
        // Do any additional setup after loading the view.
    }

    @IBAction func Motor1SliderChanged(_ sender: Any) {
        Motor1Label.text = "Motor1 = " + String(Int(Motor1Slider.value))
    }
    @IBAction func Motor2SliderChanged(_ sender: Any) {
        Motor2Label.text = "Motor2 = " + String(Int(Motor2Slider.value))
    }
    @IBAction func Motor3SliderChanged(_ sender: Any) {
        Motor3Label.text = "Motor3 = " + String(Int(Motor3Slider.value))
    }
    @IBAction func Motor4SliderChanged(_ sender: Any) {
        Motor4Label.text = "Motor4 = " + String(Int(Motor4Slider.value))
    }
    
    @IBAction func SendButtonTapped(_ sender: UIButton) {
        let json: [String: Int] = ["motor1": Int(Motor1Slider.value),
                                   "motor2": Int(Motor2Slider.value),
                                   "motor3": Int(Motor3Slider.value),
                                   "motor4": Int(Motor4Slider.value)]

        let jsonData = try? JSONSerialization.data(withJSONObject: json)

        // create post request
        let url = URL(string: "RobotArm.local")!
        var request = URLRequest(url: url)
        request.httpMethod = "POST"

        // insert json data to the request
        request.httpBody = jsonData

        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard let data = data, error == nil else {
                print(error?.localizedDescription ?? "No data")
                return
            }
            let responseJSON = try? JSONSerialization.jsonObject(with: data, options: [])
            if let responseJSON = responseJSON as? [String: Any] {
                print(responseJSON)
            }
        }

        task.resume()
        
    }
    
}

